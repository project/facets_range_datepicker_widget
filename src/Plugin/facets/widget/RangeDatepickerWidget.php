<?php

namespace Drupal\facets_range_datepicker_widget\Plugin\facets\widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\Component\Utility\Html;

/**
 * The Datepicker widget.
 *
 * @FacetsWidget(
 *   id = "range_datepicker",
 *   label = @Translation("Range Datepicker"),
 *   description = @Translation("A widget that shows a datepicker slider."),
 * )
 */
class RangeDatepickerWidget extends DatepickerWidget {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'facet_min_label' => 'Initial Date',
      'facet_max_label' => 'Closing Date',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);

    $results = $facet->getResults();
    if (empty($facet->getResults())) {
      return $build;
    }

    $active = $facet->getActiveItems();
    $config = $this->getConfiguration();

    // As this extends DatepickerWidget update the 'min' title.
    $build['#items']['min']['label']['#value'] = $config['facet_min_label'];
    $labelValue = $facet->getName();

    $htmlUniqueId = Html::getUniqueId($facet->id() . '-max');

    $build['#items']['max']['label'] = [
      '#type' => 'html_tag',
      '#tag' => 'label',
      '#value' => $config['facet_max_label'],
      '#attributes' => [
        'id' => $htmlUniqueId . '-label',
        'for' => $htmlUniqueId,
      ],
    ];
    $build['#items']['max']['input'] = [
      '#type' => 'html_tag',
      '#tag' => 'input',
      '#attributes' => [
        'type' => 'date',
        'class' => ['facet-datepicker'],
        'id' => $htmlUniqueId,
        'name' => $htmlUniqueId,
        'data-type' => 'datepicker-max',
      ],
    ];
    if ($config['labels_hidden'] == 1) {
      $build['#items']['min']['label']['#attributes']['class'] = ['visually-hidden'];
      $build['#items']['max']['label']['#attributes']['class'] = ['visually-hidden'];
    }
    $url = array_shift($results)->getUrl()->toString();
    $build['#attached']['drupalSettings']['facets']['datepicker'][$facet->id()] = [
      'url' => $url,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function isPropertyRequired($name, $type) {
    if ($name === 'range_datepicker' && $type === 'processors') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'range';
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $form += parent::buildConfigurationForm($form, $form_state, $facet);

    $message = $this->t('To achieve the standard behavior of a Range datepicker, you need to enable the facet setting below <em>"Range Datepicker"</em>.');
    $form['warning'] = [
      '#markup' => '<div class="messages messages--warning">' . $message . '</div>',
    ];
    $config = $this->getConfiguration();

    $form['facet_min_label']['#default_value'] = $config['facet_min_label'];
    $form['facet_min_label']['#title'] = $this->t('Minimum Date Label');

    $form['facet_max_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum Date Label'),
      '#default_value' => $config['facet_max_label'],
    ];
    return $form;
  }

}
