<?php

namespace Drupal\facets_range_datepicker_widget\Plugin\facets\widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Widget\WidgetPluginBase;
use Drupal\Component\Utility\Html;

/**
 * The datepicker widget.
 *
 * @FacetsWidget(
 *   id = "datepicker",
 *   label = @Translation("Datepicker"),
 *   description = @Translation("A widget that shows a datepicker."),
 * )
 */
class DatepickerWidget extends WidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'labels_hidden' => 0,
      'facet_min_label' => 'Select Date',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);

    $results = $facet->getResults();
    if (empty($results)) {
      return $build;
    }
    ksort($results);

    $active = $facet->getActiveItems();
    $default = '';
    if (isset($active[0]['min'])) {
      $default = date('Y-m-d', $active[0]['min']);
    }
    $config = $this->getConfiguration();

    $htmlUniqueId = Html::getUniqueId($facet->id() . '-min');
    $build['#items'] = [];
    $build['#items']['min']['label'] = [
      '#type' => 'html_tag',
      '#tag' => 'label',
      '#value' => $config['facet_min_label'],
      '#attributes' => [
        'id' => $htmlUniqueId . '-label',
        'for' => $htmlUniqueId,
      ],
    ];
    $build['#items']['min']['input'] = [
      '#type' => 'html_tag',
      '#tag' => 'input',
      '#attributes' => [
        'type' => 'date',
        'class' => ['facet-datepicker'],
        'id' => $htmlUniqueId,
        'name' => $htmlUniqueId,
        'data-type' => 'datepicker-min',
      ],
    ];
    if ($config['labels_hidden'] == 1) {
      $build['#items']['min']['label']['#attributes']['class'] = ['visually-hidden'];
    }

    $url = array_shift($results)->getUrl()->toString();
    $build['#attached']['drupalSettings']['facets']['datepicker'][$facet->id()] = [
      'url' => $url,
    ];

    $build['#attached']['library'][] = 'facets_range_datepicker_widget/datepicker';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $form += parent::buildConfigurationForm($form, $form_state, $facet);

    $message = $this->t('To achieve the standard behavior of a datepicker, you need to enable the facet setting below <em>"Datepicker"</em>.');
    $form['warning'] = [
      '#markup' => '<div class="messages messages--warning">' . $message . '</div>',
    ];
    $config = $this->getConfiguration();

    $form['labels_hidden'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add <q>visually-hidden</q> class to Facet Labels'),
      '#default_value' => $config['labels_hidden'],
    ];

    $form['facet_min_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date Label'),
      '#default_value' => $config['facet_min_label'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function isPropertyRequired($name, $type) {
    if (($name === 'datepicker') && $type === 'processors') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'range';
  }

}
